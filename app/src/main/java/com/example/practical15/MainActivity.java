package com.example.practical15;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.practical15.adapter.CustomRecyclerAdapter;
import com.example.practical15.databinding.ActivityMainBinding;
import com.example.practical15.model.Users;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    CustomRecyclerAdapter adapter;
    ArrayList<Users> usersArrayList = new ArrayList<>();

    @SuppressLint("NotifyDataSetChanged")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view1 = binding.getRoot();
        setContentView(view1);

        binding.btnSubmit.setOnClickListener(view -> {
            String username = binding.edUsername.getText().toString().trim();
            String phone = binding.edPhoneNumber.getText().toString().trim();
            if (username.equals("")) {
                binding.edUsername.setError(getString(R.string.username_error));
            } else if (phone.equals("")) {
                binding.edPhoneNumber.setError(getString(R.string.phone_number_error));
            } else {
                usersArrayList.add(new Users(username, phone));
                adapter = new CustomRecyclerAdapter(this, usersArrayList);
                adapter.notifyDataSetChanged();
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
                binding.recyclerView.setLayoutManager(linearLayoutManager);
                binding.recyclerView.setAdapter(adapter);
            }


        });

    }
}