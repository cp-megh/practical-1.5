package com.example.practical15.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.practical15.databinding.CustomRecyclerListBinding;
import com.example.practical15.model.Users;

import java.util.ArrayList;

public class CustomRecyclerAdapter extends RecyclerView.Adapter<CustomRecyclerAdapter.ViewHolder> {


    Context context;
    CustomRecyclerListBinding binding;
    ArrayList<Users> usersArrayList;

    public CustomRecyclerAdapter(Context context, ArrayList<Users> usersArrayList) {
        this.context = context;
        this.usersArrayList = usersArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        binding = CustomRecyclerListBinding.inflate(LayoutInflater.from(context), parent, false);
        return new ViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Users users = usersArrayList.get(position);
        holder.binding.txtUsername.setText(users.getUserName());
        holder.binding.txtPhone.setText(users.getPhoneNumber());

    }

    @Override
    public int getItemCount() {
        return usersArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CustomRecyclerListBinding binding;

        public ViewHolder(@NonNull CustomRecyclerListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }
    }
}
